#include <stdio.h>
#include "persistencia.h"
#define MAXE 30
char bandera1 = 1;
char bandera2 = 0;
struct Estudiante estudiantes[MAXE];
int contador = 0;
char c;
int menu(){
    while(bandera1){
        int opcion;
        printf("1) Registrar estudiante\n");
        printf("2) Buscar estudiante\n");
        printf("3) Borrar estudiante\n");
        printf("4) Mostrar estad�sticas del curso\n");
        printf("5) Guardar registros en archivo\n");
        printf("6) Salir\n");
        printf("Ingrese opci�n: ");
        if(bandera2){
            scanf("%*[^\n]%*c");
        }
        bandera2 =1;
        scanf("%d",&opcion);
        switch (opcion){
            case 1: registrarEstudiante();
                    break;
            case 2: buscarEstudiante();
                    break;
            case 3: borrarEstudiante();
                    break;
            case 4: mostrarEstadisticas();
                    break;
            case 5: guardarEstudiantes();
                    break;
            case 6: bandera1=0;
                    break;
        }
    }
    return 1;
}

char registrarEstudiante(){
    struct Estudiante estudiante; 
    if(contador>=MAXE){
        fprintf(stderr,"Los registros se han completado(30) ya no puede ingresar mas estudiantes.\n");
        return 0;
    }
    printf("Ingrese el nombre del estudiante: ");
    scanf("%*[^\n]%*c");
    scanf("%s",estudiante.nombre);
    printf("Ingrese la matricula del estudiante: ");
    scanf("%*[^\n]%*c");
    if(scanf("%ld",&estudiante.matricula) != 1){
        fprintf(stderr,"El dato ingresado no es correcto. Porfavor Ingrese digitos.\n");
        return 0;
    }
    printf("Ingrese la nota del primer pacial: ");
    scanf("%*[^\n]%*c");
    if(scanf("%d",&estudiante.nota1er)!=1 || estudiante.nota1er>100 || estudiante.nota1er<0){
        fprintf(stderr,"La nota ingresada no es valida. Por favor ingrese enteros del 0 al 100.\n");
        return 0;
    }
    printf("Ingrese la nota del segundo pacial: ");
    scanf("%*[^\n]%*c");
    if(scanf("%d",&estudiante.nota2do)!=1 || estudiante.nota2do>100 || estudiante.nota2do<0){
        fprintf(stderr,"La nota ingresada no es valida. Por favor ingrese enteros del 0 al 100.\n");
        return 0;
    }
    printf("Ingrese la nota mejoramiento: ");
    scanf("%*[^\n]%*c");
    if(scanf("%d",&estudiante.notaMejoramiento)!=1 || estudiante.notaMejoramiento>100 || estudiante.notaMejoramiento<0){
        fprintf(stderr,"La nota ingresada no es valida. Por favor ingrese enteros del 0 al 100.\n");
        return 0;
    }
    estudiantes[contador] = estudiante;
    contador++;
    return 1;
}
struct Estudiante validarEstudiante(long matriculaE){
    struct Estudiante estudiante;
    for(int i=0;i<contador;i++){
        if(estudiantes[i].matricula==matriculaE){
            estudiante=estudiantes[i];
            return estudiante;
        }
    }
    estudiante.matricula=0;
    return estudiante;
}
char buscarEstudiante(){
    struct Estudiante estudianteE;
    long matricula;
    printf("Ingrese la matricula del estudiante que desea buscar: ");
    scanf("%*[^\n]%*c");
    if(scanf("%ld",&matricula) != 1){
        fprintf(stderr,"El dato ingresado no es correcto. Porfavor Ingrese digitos.\n");
        return 0;
    }
    estudianteE=validarEstudiante(matricula);
    if(estudianteE.matricula == 0){
        fprintf(stderr,"El estudiante no existe. Porfavor ingrese un numero de matricula valido.\n");
        return 0;
    }
    printf("El estudiante que busca es:\n Nombre: %s\n Matricula: %ld\n",estudianteE.nombre,estudianteE.matricula);
    printf(" Nota primer parcial: %d\n Nota segundo parcial: %d\n Nota mejoramiento: %d\n",estudianteE.nota1er,estudianteE.nota2do,estudianteE.notaMejoramiento);
    return 1;
}

void eliminar (long matriculaE){
    for (int i = 0; i < contador; i++){
        if(estudiantes[i].matricula==matriculaE){
            compactarArreglo(i);
            contador--;
        }
    }
}

char compactarArreglo(int indice){
    if(indice>=contador){
        return 0;
    }
    for(indice;indice<contador;indice++){
        estudiantes[indice]=estudiantes[indice+1];
    }
    return 1;
}

char borrarEstudiante(){
    struct Estudiante estudianteE;
    long matricula;
    int confirmacion=0;
    printf("Ingrese la matricula del estudiante que desea eliminar: ");
    scanf("%*[^\n]%*c");
    if(scanf("%ld",&matricula) != 1){
        fprintf(stderr,"El dato ingresado no es correcto. Porfavor Ingrese digitos.\n");
        return 0;
    }
    estudianteE=validarEstudiante(matricula);
    if(estudianteE.matricula == 0){
        fprintf(stderr,"El estudiante no existe. Porfavor ingrese un numero de matricula valido.\n");
        return 0;
    }
    printf("Esta seguro de que desea eliminar este estudiante?Si(1) No(0): ");
    scanf("%*[^\n]%*c");
    if ((scanf("%d",&confirmacion)!= 1)||confirmacion){
    eliminar(estudianteE.matricula);
        printf("El estudiante se elimino exitosamente.\n");
        return 1;
    }
    printf("El estudiante no se ha eliminado.\n");
    return 0;
}

int calcularNotaFinal(struct Estudiante estudiantee){
    int cali1=estudiantee.nota1er;
    int cali2=estudiantee.nota2do;
    int cali3=estudiantee.notaMejoramiento;
    int promediofinal=0;
    if(cali3 > cali1 && cali2 > cali1){
        promediofinal=(cali3 + cali2)/2;
    }
    else if(cali3 > cali2 && cali1 >cali2){
        promediofinal=(cali3 + cali1)/2;
    }
    else{
        promediofinal=(cali2 + cali1)/2;
    }
    return promediofinal;
}

int promedioCurso(){
    int suma=0;
    int prom =0;
    for (int i = 0; i < contador; i++){
        suma=suma+calcularNotaFinal(estudiantes[i]);
    }
    if(contador!=0){
        prom=suma/contador;
    }
    return prom;
}

struct Estudiante estudianteMejNota(){
    int mejorNota=0;
    struct Estudiante est;
    if(contador==1){
        est=estudiantes[0];
        return est;
    }
    for (int i = 0; i < contador; i++){
        int notaActual = calcularNotaFinal(estudiantes[i]);
        if (notaActual>mejorNota){
            mejorNota=notaActual;
            est=estudiantes[i];
        }
    }
    return est;
}

struct Estudiante estudiantePeorNota(){
    int peorNota=100;
    struct Estudiante est;
    if(contador==1){
        est=estudiantes[0];
        return est;
    }
    for (int i = 0; i < contador; i++){
        int notaActual = calcularNotaFinal(estudiantes[i]);
        if (notaActual<peorNota){
            peorNota=notaActual;
            est=estudiantes[i];
        }
    }
    return est;
}

void mostrarEstadisticas(){
    if(contador!=0){
        int promedioC=promedioCurso();
        printf("El promedio general del curso es: %d\n",promedioC);
        struct Estudiante estudianteM = estudianteMejNota();
        int mejNota = calcularNotaFinal(estudianteM);
        printf("El estudiante con la mejor nota es: \n Nombre: %s\n Matricula: %ld\n",estudianteM.nombre,estudianteM.matricula);
        printf(" Con un promedio de: %d\n",mejNota);
        struct Estudiante estudianteP = estudiantePeorNota();
        int peNota = calcularNotaFinal(estudianteP);
        printf("El estudiante con la peor nota es: \n Nombre: %s\n Matricula: %ld\n",estudianteP.nombre,estudianteP.matricula);
        printf(" Con un promedio de: %d\n",peNota);
    }else{
        fprintf(stderr,"Aun no hay registros de estudiantes.\n");
    }
}

void guardarEstudiantes(){
    guardar(estudiantes);
}
