CC=gcc
CFLAGS=-c -I.

estudiante: estudiante.o main.o persistencia.o persistencia.h estudiante.h
	$(CC) -o estudiante estudiante.o main.o persistencia.o persistencia.h estudiante.h -I.

estudiante.o: estudiante.c
	$(CC) $(CFLAGS) estudiante.c

persistencia.o: persistencia.c
	$(CC) $(CFLAGS) persistencia.c

main.o: main.c
	$(CC) $(CFLAGS) main.c


.PHONY: clean
clean:
	rm *o estudiante
