#define MAX 100

struct Estudiante{
    char nombre[MAX];
    long matricula;
    int nota1er;
    int nota2do;
    int notaMejoramiento;
};
int menu();
char registrarEstudiante();
char buscarEstudiante();
struct Estudiante validarEstudiante(long);
void eliminar (long);
char compactarArreglo(int);
char borrarEstudiante();
int calcularNotaFinal(struct Estudiante);
int promedioCurso();
struct Estudiante estudianteMejNota();
struct Estudiante estudiantePeorNota();
void mostrarEstadisticas();
void guardarEstudiantes();
